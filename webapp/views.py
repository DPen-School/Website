from flask import Flask, render_template, url_for,session, request
import os
from os import listdir
from os.path import isfile, join,isdir
app = Flask(__name__)

# Config options - Make sure you created a 'config.py' file.
app.config.from_object('config')
app.secret_key = app.config['SECRET_KEY']
# To get one variable, tape app.config['MY_VARIABLE']

@app.context_processor
def options():
    def getColor():
        if 'color' in session:
            return session['color']
        else:
            return 'blue'
    
    def getHack():
        if 'hack' in session:
            return session['hack']
        else:
            return "off"
    
    def getTheme():
        if 'theme' in session:
            return session['theme']
        else:
            return 'light'
    def matiereList():
        path = os.getcwd() + "/webapp/templates/cours/"
        fold = [f for f in listdir(path) if isdir(join(path,f))]
        return fold
    return dict(getColor=getColor,getHack=getHack,getTheme=getTheme,matiereList=matiereList)

@app.route('/')
def index():
        return render_template("index.html")

@app.route('/cours/<matiere>/')
def listCours(matiere):
    path = os.getcwd() + "/webapp/templates/cours/{}".format(matiere)
    cours = [f for f in listdir(path) if isfile(join(path, f))]
    return render_template('utils/coursLister.html',matiere=matiere, coursList=cours)

@app.route('/cours/<matiere>/<cours>/')
def cours(matiere,cours):
    return render_template("utils/sampleCours.html",matiere=matiere,cours=cours)

@app.route('/option/', methods=['POST'])
def option():
    session['hack'] = request.form.get('hack')
    
    if(request.form.get('theme')=="on"):
        session['theme'] = "grey darken-4 white-text"
    else:
        session['theme'] = ""
    
    session['color'] = request.form.get('color')

    return render_template("utils/option.html")

if __name__ == "__main__":
    app.run()