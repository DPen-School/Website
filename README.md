Voici DPen un site fait pour upload des cours mais est indépendant de chaque école/classe, il permet a n'importe qui (ayant des bases en informatique) de créer un petit site de travail pour sa classe/ecole.

Le site marche aussi sur mobile mais est un peut moins rapide et l'option pour la calculatrice n'est pas implémenté.
Le site requiert un serveur avec Flask
pour le créer vous avez juste a cloner ce git puis simplement lancer run.py ce qui créera a la permière execution
un fichier config.py avec une clé secrete

Note : Sur heroku l'app ne peut pas créer de fichier donc si elle est envoyer sans config.py elle crachera donc mieux vaut générer le config.py avant de le séployer