import os
skey = os.urandom(24)

file = open("config.py","w+")
file.write("SECRET_KEY = {}".format(skey))
file.close()

import webapp
from webapp import app

if __name__ == "__main__":
    app.run(debug=True)